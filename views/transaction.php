<?php 
require "../partials/template.php";

function get_Title(){
	echo "History2";
 }
 function get_body_contents(){
 	require "../controllers/connection.php";
 	?>
 	<h1 class="text-center py-5">Order History</h1>
 	<hr class=" border-white">
 	<table class="table table-striped">
 		<thead>
 			<tr>
 				<td>Order ID</td>
 				<td>User ID</td>
 				<td>Order Details</td>
 				<td>Payment</td>
 				<td>Status</td>
 			</tr>
 		</thead>
 		<tbody>
 			<?php 

 			$userId = $_SESSION['user']['id'];
 			$order_query = "SELECT * FROM orders WHERE user_id = $userId";

 			$orders = mysqli_query($conn, $order_query);
 			foreach($orders as $indiv_order){

 			 ?>
 			<tr>
 				<td><?php echo $indiv_order['id'] ?></td>
 				<td><?php echo $userId ?></td>
 				<td>
 					<?php 
 					$order_id = $indiv_order['id'];
 					$items_query = "SELECT * FROM items JOIN item_order ON (items.id = item_order.item_id) WHERE item_order.order_id = $order_id";

 					$items = mysqli_query($conn, $items_query);

 					foreach($items as $indiv_item){

 					 ?>
 					 <span><?php echo $indiv_item['name'] ?></span><br>
 					 <?php
 					}
 					?>
 				</td>
 				<td><?php 
 				$order_id = $indiv_order['id'];
 				$payment_query = "SELECT * FROM payments JOIN orders ON (payments.id = orders.payment_id)";
 				$payments = mysqli_query($conn, $payment_query);

 				foreach($payments as $indiv_payment){
 					?>
 					<span><?php echo $indiv_payment['name'] ?></span><br>
 					<?php
 				}
 				?></td>
 			</tr>
 			<?php
 		}
 		?>
 		</tbody>
 	</table>
<?php
}
?>