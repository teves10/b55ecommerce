<?php 
require "../partials/template.php";

function get_title(){
echo "Profile";
}

function get_body_contents(){
require "../controllers/connection.php";

?>	
<h1 class="text-center py-5">Profile Page</h1>

<div class="container mb-5">
    <div class="row">
          
<div class="col-lg-4">
          <!-- add text     -->
          <form action="" method="POST">
<div class="form-group">
<label for="firstName">First Name:</label>
<a class="nav-link" href="#"><?php echo $_SESSION['user']['firstName']?></a>
</div>
<div class="form-group">
<label for="lastName">Last Name:</label>
<a class="nav-link" href="#"><?php echo $_SESSION['user']['lastName']?></a>
</div>
<div class="form-group">
<label for="email">Email:</label>
<a class="nav-link" href="#"><?php echo $_SESSION['user']['email']?></a>
</div>

<a href="history.php" type="button" class="btn btn-info">History Page</a>
</form>
</div>
<!-- Code from Ryan -->

<div class="col-lg-4">
              <h3>Address:</h3>
              <ul>
                  <?php 
                  $userId = $_SESSION['user']['id'];
                      $address_query = "SELECT * FROM addresses WHERE user_id = $userId ";
                      $addresses = mysqli_query($conn, $address_query);
                      foreach ($addresses as $indiv_address) {
                  ?>
                      <li><?php echo $indiv_address['address1'] . ", " . $indiv_address['address2']. "<br>" . $indiv_address['city'] . $indiv_address['zipcode'] ?>
                      </li>
                  <?php
                      }

                   ?>
              </ul>


<!-- End -->

<form action="../controllers/add-address-process.php" method="POST">
<div class="form-group">
<label for="address1">Address 1:</label>
<input type="text" name="address1" class="form-control">
</div>
<div class="form-group">
<label for="address2">Address 2:</label>
<input type="text" name="address2" class="form-control">
</div>
<div class="form-group">
<label for="city">City:</label>
<input type="text" name="city" class="form-control">
</div>
<div class="form-group">
<label for="zipCode">Zip Code:</label>
<input type="text" name="zipcode" class="form-control">
</div>
<input type="hidden" name="user_id" value="<?php echo $userId?>">
<button class="btn btn-info" type="submit">Add Address</button>
</form>


</div>

  <div class="col-lg-4">
    
    <h3>Contact:</h3>
     <ul>
                    <?php 
                    $userId = $_SESSION['user']['id'];
                        $contact_query = "SELECT * FROM contacts WHERE user_id = $userId ";
                        $contacts = mysqli_query($conn, $contact_query);
                        foreach ($contacts as $indiv_contact) {
                    ?>
                        <li><?php echo $indiv_contact['contactNo'] ?>
                        </li>
                    <?php
                        }

                     ?>
                </ul>

                <form action="../controllers/add-contact-process.php" method="POST">
                  <div class="form-group">
                  <label for="contactNo">Contact Number:</label>
                  <input type="number" name="contactNo" class="form-control">
                  </div>
                  <input type="hidden" name="user_id" value="<?php echo $userId?>">
                  <button class="btn btn-info" type="submit">Add Contact Number</button>
                  </form>


  </div>
</div>
</div>
</div>

<?php
}
?>