let addToCartBtns = document.querySelectorAll(".add-to-cart");

console.log(addToCartBtns);

addToCartBtns.forEach(indiv_btn=>{

	indiv_btn.addEventListener('click', btn=>{
		let id = btn.target.getAttribute("data-id");

		let quantity = btn.target.previousElementSibling.value;

		if (quantity <= 0) {
			alert("Please enter valid quantity");
		}else{
			 let data = new FormData;

			 data.append("id", id);
			 data.append("cart", quantity);

			 fetch("../../controllers/add-to-cart-process.php",{
			 	method: "POST",
			 	body: data
			 })
			 .then(response=>{
			 	return response.text();
			 })
			 .then(res=>{
			 	document.getElementById('cartCount').textContent = res;
			 })
		}

	})
})

// console.log(indiv_btn);

